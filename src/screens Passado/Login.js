
import React, { useState, useEffect } from 'react';
import {View, Image } from 'react-native'
import SignIn from '../components/login/SignIn';


export default function Login({navigation}) {
    return (
        <View>
            <SignIn navigation={navigation}  />
        </View>
    );
}
