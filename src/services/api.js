import axios from 'axios';

const api = axios.create({
    
    baseURL: 'https://matchateam.herokuapp.com/',
})

export default api;