import axios from 'axios';


export default class Api {
    constructor(baseURL){
        this.axios = axios.create({
            baseURL,
            headers: {
                'accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer `
            }
        });
    }
};
 