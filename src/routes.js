import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import login from './pages/login/index';
import cadastro from './pages/cadastro/index';
import home from './pages/home/index';
import cadastroCol from './pages/cadastroCol/index';
import homeCol from './pages/homeCol/index';
import cadastroQuadra from './pages/cadastroQuadra/index';
import criaSala from './pages/criaSala/index';
import sala from './pages/sala/index';
import salaPer from './pages/salaPer/index';


const Stack = createStackNavigator();

function Routes() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Login">
                <Stack.Screen name="login"
                    component={login}
                    options={{
                        title: 'Matchateam',
                        headerTitleAlign: 'center',
                        headerStyle: {
                            backgroundColor: 'black'
                        },
                        headerTintColor: 'white',
                    }}
                />
                <Stack.Screen name="cadastro"
                    component={cadastro}
                    options={{
                        title: 'Cadastro',
                        headerTitleAlign: 'center',
                        headerStyle: {
                            backgroundColor: 'white'
                        },
                        headerTintColor: 'black',
                    }}
                />

                <Stack.Screen name="home"
                    component={home}
                    options={{
                        title: 'Home',
                        headerTitleAlign: 'center',
                        headerStyle: {
                            backgroundColor: 'white'
                        },
                        headerTintColor: 'black',
                        headerLeft: null
                    }}
                />

                <Stack.Screen name="cadastroCol"
                    component={cadastroCol}
                    options={{
                        title: 'Cadastro Colaborador',
                        headerTitleAlign: 'center',
                        headerStyle: {
                            backgroundColor: 'white'
                        },
                        headerTintColor: 'black',
                    }}
                />
                <Stack.Screen name="homeCol"
                    component={homeCol}
                    options={{
                        title: 'Home',
                        headerTitleAlign: 'center',
                        headerStyle: {
                            backgroundColor: 'white'
                        },
                        headerTintColor: 'black',
                    }}
                />
                <Stack.Screen name="cadastroQuadra"
                    component={cadastroQuadra}
                    options={{
                        title: 'Cadastrar Quadra',
                        headerTitleAlign: 'center',
                        headerStyle: {
                            backgroundColor: 'white'
                        },
                        headerTintColor: 'black',
                    }}
                />
                <Stack.Screen name="criaSala"
                    component={criaSala}
                    options={{
                        title: 'Criar Sala',
                        headerTitleAlign: 'center',
                        headerStyle: {
                            backgroundColor: 'white'
                        },
                        headerTintColor: 'black',
                    }}
                />
                <Stack.Screen name="sala"
                    component={sala}
                    options={{
                        title: 'sala',
                        headerTitleAlign: 'center',
                        headerStyle: {
                            backgroundColor: 'white'
                        },
                        headerTintColor: 'black',
                    }}
                />
                <Stack.Screen name="salaPer"
                    component={salaPer}
                    options={{
                        title: 'sala',
                        headerTitleAlign: 'center',
                        headerStyle: {
                            backgroundColor: 'white'
                        },
                        headerTintColor: 'black',
                    }}
                />

            </Stack.Navigator>
        </NavigationContainer>
    );
};



export default Routes;