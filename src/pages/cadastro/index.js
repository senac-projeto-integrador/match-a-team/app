import React, { useEffect, useState } from 'react'
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Text,
    KeyboardAvoidingView,
    CheckBox,
    Picker,
    ScrollView
} from 'react-native';

import DatePicker from 'react-native-datepicker';
import styles from './styles';
import TextInputMask from 'react-native-text-input-mask';
import api from '../../services/api';
//import { ScrollView } from 'react-native-gesture-handler';
export default function cadastro({ navigation }) {


    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);
    const [name, setName] = useState(null);
    const [username, setUsername] = useState(null);
    const [type, setType] = useState(1);
    const [is_active, setIs_active] = useState(1);
    const [date_birth, setDate_birth] = useState(null);
    const [phone, setPhone] = useState(null);
    const [cpf, setCpf] = useState(null);
    const [score, setScore] = useState(0.0);
    const [conSenha, setConsenha] = useState(null);
    const [conWar, setConWar] = useState(null);
    const [gener, setGener] = useState(1);


    function  validate() {
        if (!email) {
            alert("Preencha o email corretamente")
            return false
        }
        if (!password) {
            alert("Preencha o password corretamente")
            return false
        }
        if (password != conSenha) {
            alert("O password esta diferente")
            return false
        }
        if (!username) {
            alert("Preencha o username corretamente")
            return false
        }
        if (!name) {
            alert("Preencha o nome corretamente")
            return false
        }
        if (!phone) {
            alert("Preencha o telefone corretamente")
            return false
        }
        if (!cpf) {
            alert("Preencha o cpf corretamente")
            return false
        }
        if (!date_birth) {
            alert("Preencha a data de nascimento corretamente")
            return false
        }
        return true
    }

    
    
    const doRegister = () => {



        if (validate() != false) {
            const body = {
                gener,
                type,
                name,
                is_active,
                username,
                password,
                date_birth,
                phone,
                email,
                cpf,
                score
            };



            try {
                api.post('users', body, {
                }).then(response => {
                    alert("Usuario Cadastrado!")
                    navigation.navigate("login");
                })
            } catch {
                alert("Usuario ja existente")
            }

        }

    };


    return (

        <ScrollView style={styles.background}>
            <KeyboardAvoidingView>


                <View style={styles.view}>
                    <TextInput style={styles.input}
                        placeholder="Nome Completo"
                        autoCorrect={true}
                        onChangeText={(e) => { setName(e) }}
                    />

                    <TextInput style={styles.input}
                        placeholder="User Name"
                        autoCorrect={true}
                        onChangeText={(e) => { setUsername(e) }}
                    />

                    <TextInput style={styles.input}
                        placeholder="Email"
                        keyboardType = "email-address"
                        autoCorrect={true}
                        onChangeText={(e) => { setEmail(e) }}
                    />

                    <TextInputMask style={styles.input}
                        placeholder="Telefone"
                        mask={"([00])[000][00]-[00][00]"}
                        keyboardType = "number-pad"
                        autoCorrect={true}
                        onChangeText={(e) => { setPhone(e) }}
                    />

                    <TextInputMask style={styles.input}
                        placeholder="Cpf"
                        mask={"[000].[000].[000]-[00]"}
                        keyboardType = "number-pad"
                        autoCorrect={false}
                        onChangeText={(e) => { setCpf(e) }}
                    />

                    <TextInput style={styles.input}
                        placeholder="Senha"
                        autoCorrect={false}
                        secureTextEntry
                        onChangeText={(e) => { setPassword(e) }}
                    />

                    <TextInput style={styles.input}
                        placeholder="Confirma Senha"
                        autoCorrect={false}
                        secureTextEntry
                        onChangeText={(e) => { setConsenha(e) }}
                    />

                    <View style={styles.viewtt}>
                        <Text style={styles.combotext}>Sexo: </Text>

                        <Picker
                            selectedValue={gener}
                            style={styles.combo}
                            onValueChange={(itemValue, itemIndex) => setGener(itemValue)}
                        >
                            <Picker.Item label="Homem" value="1" />
                            <Picker.Item label="Mulher" value="2" />
                        </Picker>

                    </View>


                    <DatePicker
                        style={{ width: 200, marginTop: 5, }}
                        date={date_birth}
                        mode="date" //The enum of date, datetime and time
                        placeholder="Data de nascimento"
                        format="YYYY-MM-DD"
                        minDate="1952-01-01"
                        maxDate="3000-01-01"
                        confirmBtnText="ok"
                        cancelBtnText="Cancel"
                        customStyles={{
                            dateIcon: {
                                position: 'absolute',
                                left: 0,
                                top: 4,
                                marginLeft: 0
                            },
                            dateInput: {
                                marginLeft: 36,
                                backgroundColor: '#00BFFF'
                            }
                        }}
                        onDateChange={(date) => setDate_birth(date)}
                    />



                    <TouchableOpacity style={styles.btnSubmit} onPress={() => doRegister()} >
                        <Text >Registrar </Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.btnRegister} onPress={() => navigation.navigate("cadastroCol")} >
                    <Text style={styles.btnRegisterText}>Cadastre sua Quadra</Text>
                </TouchableOpacity>
                </View>



            </KeyboardAvoidingView>
        </ScrollView>

    )
}


