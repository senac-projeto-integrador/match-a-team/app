import React, { useEffect, useState } from 'react'
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Text,
    KeyboardAvoidingView,
    CheckBox,
    Picker,
    ScrollView,
    SafeAreaView,
    FlatList
} from 'react-native';

import { Container, Button } from 'native-base';

import DatePicker from 'react-native-datepicker';
import styles from './styles';
import TextInputMask from 'react-native-text-input-mask';
import api from '../../services/api';
import AsyncStorage from '@react-native-community/async-storage';
//import { ScrollView } from 'react-native-gesture-handler';
export default function cadastro({ navigation }) {


    const [name, setName] = useState(null);
    const [dataPart, setDataPart] = useState(null);
    const [description, setDescription] = useState(null);
    const [horarioPart, setHorarioPart] = useState(null);
    const [tempoJogo, setTempoJogo] = useState(null);
    const [user, setUser] = useState(null);
    const [matchs_id, setMatchs_id] = useState(null);


    const [gyms, setGyms] = useState(null);
    const [gymsId, setGymsId] = useState(null);
    const [gymsName, setGymsName] = useState(null);


    function validate() {
        if (!name) {
            alert("Preencha o nome da sala")
            return false
        }
        if (!description) {
            alert("Preencha a descrição corretamente")
            return false
        }
        if (!dataPart) {
            alert("Preencha a data da partida corretamente")
            return false
        }
        if (!horarioPart) {
            alert("Preencha o horario da partida corretamente")
            return false
        }

        return true
    }


    const getToken = async () => {
        const userToken = await AsyncStorage.getItem('token');
        const userId = await AsyncStorage.getItem('userId');


        api.get(`/users/${userId}`, {
            headers: {
                'Authorization': `Bearer ` + userToken
            }
        })
            .then(response => {
                setUser(response.data)
            })

        api.get(`/gyms/all/`, {
            headers: {
                'Authorization': `Bearer ` + userToken
            }
        })
            .then(response => {
                setGyms(response.data)

            })




    }




    useEffect(() => {

        if (!user) {
            getToken();
        }


    });






    const setaIdGym = async (id) => {


        const userToken = await AsyncStorage.getItem('token');

        api.get(`/gyms/` + id, {
            headers: {
                'Authorization': `Bearer ` + userToken
            }
        })
            .then(response => {
                setGymsName(response.data)

            })

    };

    const seta = async (id) => {

        try {
            const userId = await AsyncStorage.getItem('userId')
            const userToken = await AsyncStorage.getItem('token');


            const body = {
                Matchs_id: id,
                users_id: userId,
            }
            console.log("AQUI" + id)

            AsyncStorage.setItem("matchId", id.toString());
            api.post('matchs/inmatch', body, {
                headers: {
                    'Authorization': `Bearer ` + userToken
                }

            }).then(response => console.log(response.data))

            alert("Sala Criada!")



            navigation.navigate("sala");
        } catch (error) {
            console.log(error)
        }


    }



    const doRegister = async () => {



        if (validate() != false) {
            const body = {
                gyms_id: gymsName[0].id,
                courts_id: 1,
                name,
                start_time: "00:00",
                end_time: "00:00",
                type: 1,
                description,
                responsible_id: userId = await AsyncStorage.getItem('userId'),
                dataPart,
                horarioPart,
                tempoJogo
            };


            const userToken = await AsyncStorage.getItem('token');
            try {
                api.post('matchs', body, {
                    headers: {
                        'Authorization': `Bearer ` + userToken
                    }

                }).then(response => {

                    setMatchs_id(response.data.id)
                    console.log("ID DO CRIA SALA INMATCH " + response.data.id)
                    
                    seta(response.data.id);

                    // navigation.navigate("home");
                })


            } catch {
                alert("Sala ja existente")
            }

        }




    };


    return (

        <Container style={{ backgroundColor: 'black' }}>




            <View style={styles.view}>
                <TextInput style={styles.input}
                    placeholder="Nome"
                    autoCorrect={true}
                    onChangeText={(e) => { setName(e) }}
                />

                <TextInput style={styles.input}
                    placeholder="Descrição"
                    autoCorrect={true}
                    onChangeText={(e) => { setDescription(e) }}
                />


                <DatePicker
                    style={{ width: 200, marginTop: 5, marginBottom: 10 }}
                    date={dataPart}
                    mode="date" //The enum of date, datetime and time
                    placeholder="Dia do jogo"
                    format="YYYY-MM-DD"
                    minDate="1952-01-01"
                    maxDate="3000-01-01"
                    confirmBtnText="ok"
                    cancelBtnText="Cancel"
                    customStyles={{
                        dateIcon: {
                            position: 'absolute',
                            left: 0,
                            top: 4,
                            marginLeft: 0
                        },
                        dateInput: {
                            marginLeft: 36,
                            backgroundColor: '#00BFFF'
                        }
                    }}
                    onDateChange={(date) => setDataPart(date)}
                />

                <TextInputMask style={styles.input}
                    placeholder="Horario do jogo"
                    mask={"[00]:[00]"}
                    keyboardType="number-pad"
                    autoCorrect={true}
                    onChangeText={(e) => { setHorarioPart(e) }}
                />

                <TextInputMask style={styles.input}
                    placeholder="Duração do jogo"
                    mask={"[0]:[00]"}
                    keyboardType="number-pad"
                    autoCorrect={true}
                    onChangeText={(e) => { setTempoJogo(e) }}
                />


                <Text style={{ color: 'white', fontSize: 12 }}>
                    Selecionado: {gymsName && gymsName[0].name}
                </Text>


                <View style={[{ backgroundColor: 'black', width: 412, alignItems: 'center' }]}>


                    <SafeAreaView style={{ height: 200 }}>
                        <FlatList
                            data={gyms}
                            keyExtractor={item => item.id}
                            numColumns={1}
                            renderItem={({ item }) => {
                                return (
                                    <View style={{ maxWidth: 400 }}>
                                        <Button onPress={() => setaIdGym(item.id)} style={{ margin: 3, flexDirection: 'column', height: 30, width: 250, borderRadius: 10 }}>
                                            <Text style={{ fontSize: 12, height: 30 }}>{item.name}</Text>
                                        </Button>
                                    </View>
                                );
                            }}
                        />
                    </SafeAreaView>


                </View>









                <TouchableOpacity style={styles.btnSubmit} onPress={() => doRegister()} >
                    <Text >Criar Sala </Text>
                </TouchableOpacity>
            </View>




        </Container>


    )
}


