import React, { useEffect, useState } from 'react'
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Text,
    KeyboardAvoidingView,
    CheckBox,
    Picker,
    ScrollView
} from 'react-native';

import DatePicker from 'react-native-datepicker';
import styles from './styles';
import TextInputMask from 'react-native-text-input-mask';
import api from '../../services/api';
//import { ScrollView } from 'react-native-gesture-handler';
export default function cadastro({ navigation }) {


    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);
    const [name, setName] = useState(null);
    const [username, setUsername] = useState(null);
    const [type, setType] = useState(2);
    const [is_active, setIs_active] = useState(1);
    const [date_birth, setDate_birth] = useState("2020-06-24");
    const [phone, setPhone] = useState(null);
    const [cpf, setCpf] = useState("000");
    const [score, setScore] = useState(0.0);
    const [conSenha, setConsenha] = useState(null);
    const [conWar, setConWar] = useState(null);
    const [gener, setGener] = useState(1);


    function  validate() {
        if (!email) {
            alert("Preencha o email corretamente")
            return false
        }
        if (!password) {
            alert("Preencha o password corretamente")
            return false
        }
        if (password != conSenha) {
            alert("O password esta diferente")
            return false
        }
        if (!username) {
            alert("Preencha o username corretamente")
            return false
        }
        if (!name) {
            alert("Preencha o nome corretamente")
            return false
        }
        if (!phone) {
            alert("Preencha o telefone corretamente")
            return false
        }
        if (!cpf) {
            alert("Preencha o cpf corretamente")
            return false
        }
        if (!date_birth) {
            alert("Preencha a data de nascimento corretamente")
            return false
        }
        return true
    }

    
    
    const doRegister = () => {



        if (validate() != false) {
            const body = {
                gener,
                type,
                name,
                is_active,
                username,
                password,
                date_birth,
                phone,
                email,
                cpf,
                score
            };



            try {
                api.post('users', body, {
                }).then(response => {
                    alert("Usuario Cadastrado!")
                    navigation.navigate("login");
                })
            } catch {
                alert("Usuario ja existente")
            }

        }

    };


    return (

        <ScrollView style={styles.background}>
            <KeyboardAvoidingView>


                <View style={styles.view}>
                    <TextInput style={styles.input}
                        placeholder="Nome Completo"
                        autoCorrect={true}
                        onChangeText={(e) => { setName(e) }}
                    />

                    <TextInput style={styles.input}
                        placeholder="User Name"
                        autoCorrect={true}
                        onChangeText={(e) => { setUsername(e) }}
                    />

                    <TextInput style={styles.input}
                        placeholder="Email"
                        keyboardType = "email-address"
                        autoCorrect={true}
                        onChangeText={(e) => { setEmail(e) }}
                    />

                    <TextInputMask style={styles.input}
                        placeholder="Telefone"
                        mask={"([00])[000][00]-[00][00]"}
                        keyboardType = "number-pad"
                        autoCorrect={true}
                        onChangeText={(e) => { setPhone(e) }}
                    />

                    <TextInput style={styles.input}
                        placeholder="Senha"
                        autoCorrect={false}
                        secureTextEntry
                        onChangeText={(e) => { setPassword(e) }}
                    />

                    <TextInput style={styles.input}
                        placeholder="Confirma Senha"
                        autoCorrect={false}
                        secureTextEntry
                        onChangeText={(e) => { setConsenha(e) }}
                    />


                    <TouchableOpacity style={styles.btnSubmit} onPress={() => doRegister()} >
                        <Text >Registrar </Text>
                    </TouchableOpacity>
                </View>



            </KeyboardAvoidingView>
        </ScrollView>

    )
}


