import React, { useEffect, useState } from 'react'
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Text,
    KeyboardAvoidingView,

} from 'react-native';

import styled from 'styled-components';
import api from '../../services/api';
//import Login from '../../services/api/Login'
import styles from './styles';

import AsyncStorage from '@react-native-community/async-storage';


const StyledImage = styled(Image)`
    width: 30;
    height: 35;
`;

export default function login({ navigation }) {


    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);



    const doLogin = async () => {
        const body = {
            email,
            password
        }
        try {
            const { data } = await api.post('/users/login/', body, {})

            await AsyncStorage.setItem("userId", data.id.toString());
            await AsyncStorage.setItem("token", data.token.toString());

            if (data.type != 1) {
                navigation.navigate("homeCol");
            } else {
                navigation.navigate("home");
            }

        } catch (error) {
            alert("Email ou senha incorreto")
        }




    };




    // camelCase
    // PascalCase
    // snake_case
    // kebab-case


    return (

        <KeyboardAvoidingView style={styles.background}>

            <View style={styles.containerLogo}>
                <Image
                    style={styles.image}
                    source={require('../../assets/logo.png')}
                />
            </View>



            <View style={styles.view}>
                <TextInput style={styles.input}
                    placeholder="Email"
                    autoCorrect={false}
                    keyboardType="email-address"
                    onChangeText={(e) => { setEmail(e) }}
                />
                <TextInput style={styles.input}
                    placeholder="Senha"
                    autoCorrect={false}
                    secureTextEntry
                    onChangeText={(e) => { setPassword(e) }}
                />

                <TouchableOpacity onPress={() => doLogin()} style={styles.btnSubmit} >
                    <Text >Acessar</Text>
                </TouchableOpacity>


                <TouchableOpacity style={styles.btnRegister} onPress={() => navigation.navigate("cadastro")} >
                    <Text style={styles.btnRegisterText}>Criar Conta</Text>
                </TouchableOpacity>
            </View>

        </KeyboardAvoidingView>

    )
}

