import React, { useEffect, useState } from 'react'
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Text,
    KeyboardAvoidingView,
    CheckBox,
    Picker,
    ScrollView
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import DatePicker from 'react-native-datepicker';
import styles from './styles';
import TextInputMask from 'react-native-text-input-mask';
import api from '../../services/api';
//import { ScrollView } from 'react-native-gesture-handler';
export default function cadastro({ navigation }) {


  /*  const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);*/
    const [name, setName] = useState(null);
   // const [users_id, setUser_id] = useState(null);
    /*const [type, setType] = useState(2);
    const [is_active, setIs_active] = useState(1);
    const [date_birth, setDate_birth] = useState("2020-06-24");
    const [phone, setPhone] = useState(null);
    const [cpf, setCpf] = useState("000");
    const [score, setScore] = useState(0.0);
    const [conSenha, setConsenha] = useState(null);
    const [conWar, setConWar] = useState(null);
    const [gener, setGener] = useState(1);
*/

    function  validate() {
        if (!name) {
            alert("Preencha o nome corretamente")
            return false
        }       
        return true
    }

    
    
    const doRegister = async () => {



        if (validate() != false) {
           
            const userId = await AsyncStorage.getItem('userId');
            const userToken = await AsyncStorage.getItem('token');
           
           
            const body = {
                name,
                is_active: 1,
                score: 0.0,
                initial_function_time: "1998-01-19 12:30:01",
                end_function_time: "1998-01-19 18:30:01",
                users_id: userId

            };



            try {
                api.post('gyms', body, {
                    headers: {
                        'Authorization': `Bearer ` + userToken
                      }
                }).then(response => {
                    alert("Ginasio Cadastrado!")
                    navigation.navigate("homeCol");
                })
            } catch {
                alert("Usuario ja existente")
            }

        }

    };


    return (

        <ScrollView style={styles.background}>
            <KeyboardAvoidingView>


                <View style={styles.view}>
                    <TextInput style={styles.input}
                        placeholder="Nome do Ginasio"
                        autoCorrect={true}
                        onChangeText={(e) => { setName(e) }}
                    />



                    <TouchableOpacity style={styles.btnSubmit} onPress={() => doRegister()} >
                        <Text >Registrar </Text>
                    </TouchableOpacity>
                </View>



            </KeyboardAvoidingView>
        </ScrollView>

    )
}


