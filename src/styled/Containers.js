import styled from 'styled-components';
import { View, Dimensions } from 'react-native'

export const Container = styled(View)`
    width: ${Dimensions.get('window').width};
    height: ${Dimensions.get('window').height};
    /* background: #000; */
    background: ${props => props.background};
    margin-top: ${props => props.marginTop};
`;

export const CenteredContainer = styled(Container)`
    align-items: center;
`;


Container.defaultProps = {
    background: 'white',
    marginTop: 0
};