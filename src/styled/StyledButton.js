import styled from 'styled-components'
import { View } from 'react-native'

const StyledButton = styled(View)`
    border-color: ${({ borderColor }) => borderColor};
    border-radius: ${ ({ borderRadius }) => borderRadius};
    background-color: ${ ({ bgColor }) => bgColor};
    color: ${ ({ color }) => color};
    padding-vertical: ${ ({ paddingVertical }) => paddingVertical};
    padding-horizontal: ${ ({ paddingHorizontal }) => paddingHorizontal};
    margin-vertical: ${ ({ marginVertical }) => marginVertical};
    margin-horizontal: ${ ({ marginHorizontal }) => marginHorizontal};
    width: ${ ({ width }) => width};
    margin-top: ${ ({ marginTop }) => marginTop};
    margin-bottom: ${ ({ marginBottom }) => marginBottom};
    align-items: center;
    justify-content: center;
`
StyledButton.defaultProps = {
    borderWidth: 2,
    borderColor: '#1766E3',
    borderRadius: 25,
    paddingVertical: 12,
    paddingHorizontal: 20,
    marginVertical: 10,
    marginHorizontal: 0,
    bgColor: '#1766E3',
    color: '#fff',
    width: '100%',
    marginTop: 0,
    marginBottom: 0,
}

export default StyledButton
