import {Text} from 'react-native';
import styled from 'styled-components';

const StyledText = styled(Text)`
    color: ${props => props.color};
    
    ${ ({bold}) => bold && `
        font-weight: bold;
    `}
`;

StyledText.defaultProps = {
    color: 'red'
};

export default StyledText;